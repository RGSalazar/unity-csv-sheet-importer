namespace UnitySheetImporter
{

    public static class Constants
    {

        public const string EDITOR_WINDOW_PARENT = "Unity Sheet Importer /";
        public const string EDITOR_WINDOW_SAMPLE = EDITOR_WINDOW_PARENT + "Sample /";

    }

}