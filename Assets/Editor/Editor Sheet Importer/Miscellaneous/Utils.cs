using UnityEditor;

namespace UnitySheetImporter
{

    public static class Utils
    {

        public static bool HasAllValues(string[] array)
        {
            if (array == null || array.Length == 0)
            {
                return false;
            }

            foreach (var entry in array)
            {
                if (string.IsNullOrEmpty(entry))
                {
                    return false;
                }
            }

            return true;
        }

        public static void PrintConsoleHelpBox()
        {
            EditorGUILayout.HelpBox(
                "View the operation's logs on the Console Window. " +
                "Visit menu: 'Window > General > Console' to see it.",
                MessageType.Info);
        }

    }

}