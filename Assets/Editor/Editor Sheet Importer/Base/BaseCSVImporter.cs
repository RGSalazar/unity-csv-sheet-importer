using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnitySheetImporter
{

	/// <summary>
	/// This is the EditorWindow parent class for configuring
	/// ScriptableObject T with sheet values represented by U model class.
	/// </summary>
	/// <typeparam name="T">The ScriptableObject descendant class that 
	/// will be configured with U values.</typeparam>
	/// <typeparam name="U">The class that represents the values 
	/// from a sheet row.</typeparam>
    public abstract class BaseCSVImporter<T, U> : BaseCSVReader 
		where T : ScriptableObject 
		where U : class
    {

        #region Private Fields

        private T configFile;
		private readonly List<U> listSheetValues = new List<U>();

        #endregion //Private Fields

        #region Class Abstracts Methods

		/// <summary>
		/// The operation to convert the values in a sheet row into 
		/// a model class for later SO storage.
		/// </summary>
		/// <param name="sheetValue">The string array that represents 
		/// the values in the sheet row.</param>
		/// <returns>The parsed sheet value into a model class that will be
		/// stored in the Config ScriptableObject.</returns>
        protected abstract U CreatSheetValue(string[] sheetValue);

		/// <summary>
		/// The operation after all sheet rows have been read.
		/// Use this for storing all of the converted sheet values 
		/// into the ScriptableObject.
		/// </summary>
		/// <param name="so">The ScriptableObject to be configured</param>
		/// <param name="sheetValueList">The list of converted sheet values.</param>
		protected abstract void ConfigureSO(T so, List<U> sheetValueList);

		#endregion //Class Abstract Methods

		#region EditorWindow Implementation

		protected override void DrawFields()
		{
			EditorGUILayout.BeginHorizontal();

			configFile = (T)EditorGUILayout.ObjectField(
				"Scriptable Object to Update", configFile, typeof(T), false);

			EditorGUILayout.EndHorizontal();

			base.DrawFields();
		}

		#endregion

		#region BaseCSVReader Implementation

		protected override bool IsRuntimeOnly() => false;

		protected override bool HasMissingFields() =>
			(configFile == null) || (string.IsNullOrEmpty(filePath));

		protected override void OnReadCSVEntry(string[] entry)
		{	
			listSheetValues.Add(CreatSheetValue(entry));

			AddProcessedLineCount();
			FinishCurrentLine();
		}

		protected override void OnReadCSVFirstRow(string[] firstRow)
		{
			listSheetValues.Clear();

			Debug.Log($"{GetType().Name}: <color=lime>" +
				$"Column Names in CSV:</color> {string.Join(" | ", firstRow)}");
			FinishCurrentLine();
		}

		protected override void OnFinishReadCSV()
		{
			ConfigureSO(configFile, listSheetValues);

			EditorUtility.SetDirty(configFile);
			AssetDatabase.SaveAssets();

			EditorGUIUtility.PingObject(configFile);
			Selection.activeObject = configFile;

			base.OnFinishReadCSV();

			Debug.Log($"{GetType().Name}: <color=lime>{listSheetValues.Count} " +
				$"sheet entries saved to SO '{configFile.name}'</color>",
				configFile);
		}

		#endregion

	}

}