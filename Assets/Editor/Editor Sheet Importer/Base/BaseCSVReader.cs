using marijnz.EditorCoroutines;
using System;
using System.Collections;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnitySheetImporter
{

	public abstract class BaseCSVReader : EditorWindow
	{

		protected const string BUTTON_TEXT = "IMPORT";

		#region Private Fields

		private bool isCurrentLineFinished = true;

		private int processedLineCount;

		protected string filePath = string.Empty;

		#endregion

		#region Accessors

		protected int CurrentRowInCSV { get; private set; }

		protected bool IsOperationFinished { get; private set; } = true;

		#endregion

		#region Abstract Methods

		/// <summary>
		/// Returns the text of the terminal button for this window.
		/// </summary>
		protected virtual string GetButtonText() => BUTTON_TEXT;

		/// <summary>
		/// Determines whether or not this EditorWindow is only during runtime.
		/// If set to false, this Window is available regardless of Editor state.
		/// </summary>
		protected abstract bool IsRuntimeOnly();

		protected abstract void OnReadCSVEntry(string[] entry);

		protected abstract void OnReadCSVFirstRow(string[] firstRow);

		#endregion

		#region EditorWindow Implementation

		protected virtual void OnGUI()
		{
			if (IsRuntimeOnly() && !EditorApplication.isPlaying)
			{
				EditorGUILayout.HelpBox(
					"Sorry, this tool is only usable during runtime in Unity Editor.",
					MessageType.Warning);
				return;
			}

			DrawFields();
		}

		#endregion

		#region Class Implementation

		protected void AddProcessedLineCount()
		{
			processedLineCount++;
		}

		protected void FinishCurrentLine()
		{
			isCurrentLineFinished = true;
		}

		protected void StopOperation()
		{
			IsOperationFinished = true;
		}

		protected void DisplayLoadingInfo()
		{
			EditorGUILayout.HelpBox("Operation in progress... Please wait until done.", MessageType.Warning);
		}

		protected virtual bool HasMissingFields() => false;

		protected virtual void OnFinishReadCSV()
		{
			var color = (processedLineCount == 0) ? "red" : "lime";
			Debug.Log($"{GetType().Name}: <color={color}>" +
				$"ENTRIES PROCESSED: {processedLineCount}</color>");

			StopOperation();
		}

		protected virtual void DrawFields()
		{
			EditorGUILayout.BeginHorizontal();
			filePath = EditorGUILayout.TextField("CSV File Path", filePath);

			GUI.backgroundColor = Color.cyan;
			if (GUILayout.Button("...", GUILayout.Width(24)))
			{
				filePath = EditorUtility.OpenFilePanel("Choose a CSV file to read",
					string.IsNullOrEmpty(filePath) ? Application.dataPath : filePath, "csv");
			}

			EditorGUILayout.EndHorizontal();

			if (string.IsNullOrEmpty(filePath))
			{
				EditorGUILayout.HelpBox("Please choose a CSV file.", MessageType.Info);
				return;
			}

			Utils.PrintConsoleHelpBox();

			if (!IsOperationFinished)
			{
				DisplayLoadingInfo();
				return;
			}

			GUI.backgroundColor = Color.green;
			if (GUILayout.Button(GetButtonText()))
			{
				if (HasMissingFields())
				{
					Debug.LogError($"{GetType().Name} Missing Editor Window Fields!");
				}
				else
				{
					this.StartCoroutine(CorReadCSVFile());
				}
			}
		}

		private IEnumerator CorReadCSVFile()
		{
			Debug.Log($"{GetType().Name}: <color=yellow>------------ " +
				$"OPERATION STARTED. ------------</color>");

			IsOperationFinished = false;
			isCurrentLineFinished = false;
			processedLineCount = 0;
			CurrentRowInCSV = 0;

			StreamReader strReader = null;

			try
			{
				strReader = new StreamReader(filePath);
			}
			catch (Exception)
			{
				Debug.LogError($"{GetType().Name}: " +
					$"<color=red>INVALID CSV FILE PATH.</color>: {filePath}");
				StopOperation();
			}

			while (!IsOperationFinished)
			{
				isCurrentLineFinished = false;
				string line = strReader.ReadLine();
				var row = string.IsNullOrEmpty(line) ? new string[] { } : line.Split(',');

				if (row.Length == 0 ||
					Array.TrueForAll(row, x => string.IsNullOrEmpty(x)))
				{
					Debug.Log($"{GetType().Name}: Row {CurrentRowInCSV} is BLANK. <color=orange>Stopping Operation.</color>");
					StopOperation();
					break;
				}

				if (CurrentRowInCSV > 0)
				{
					OnReadCSVEntry(row);
				}
				else
				{
					OnReadCSVFirstRow(row);
				}

				while (!isCurrentLineFinished)
				{
					yield return null;
				}

				CurrentRowInCSV++;
			}

			OnFinishReadCSV();
			Debug.Log($"{GetType().Name}: <color=yellow>------------ " +
				$"OPERATION FINISHED. ------------</color>");
			Repaint();
		}

		#endregion
	}

}