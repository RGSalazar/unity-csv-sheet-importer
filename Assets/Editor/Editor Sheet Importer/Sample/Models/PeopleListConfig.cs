using System.Collections.Generic;
using UnityEngine;

namespace UnitySheetImporter.Sample
{

    [CreateAssetMenu(fileName = "People List Config", 
        menuName = Constants.EDITOR_WINDOW_SAMPLE + "New People List")]
    public class PeopleListConfig : ScriptableObject
    {

        public List<PersonModel> listPeople;

    }

}