namespace UnitySheetImporter.Sample
{

    [System.Serializable]
    public class PersonModel
    {

        public string name;

        public int age;

        public string country;

        public string hobby;

        public PersonModel(string name, int age, string country, string hobby)
        {
            this.name = name;
            this.age = age;
            this.country = country;
            this.hobby = hobby;
        }

    }

}