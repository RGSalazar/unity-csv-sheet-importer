using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnitySheetImporter.Sample
{

    public class PeopleCSVImporter : BaseCSVImporter<PeopleListConfig, PersonModel>
    {

        private const string WINDOW_TITLE = "People CSV Importer"; //change value

        #region EditorWindow Implementation

        [MenuItem(Constants.EDITOR_WINDOW_SAMPLE + WINDOW_TITLE)]
        private static void ShowWindow()
        {
            //Take note of the class referenced by GetWindow
            GetWindow<PeopleCSVImporter>(false, WINDOW_TITLE, true);
        }

        #endregion

        #region BaseCSVImporter Implementation

        protected override void ConfigureSO(
            PeopleListConfig config, List<PersonModel> sheetValueList)
        {
            config.listPeople = sheetValueList;
        }

        protected override PersonModel CreatSheetValue(string[] sheetValue)
        {
            PersonModel result = null;

            try
            {
                result = new PersonModel(
                    sheetValue[0], int.Parse(sheetValue[1]), 
                    sheetValue[2], sheetValue[3]);
            }
            catch (Exception)
            {
                Debug.LogWarning($"{GetType().Name}: " +
                $"Failed to parse entry at row: {CurrentRowInCSV} = " +
                $"{string.Join(" | ", sheetValue)}");
            }

            return result;
        }

        #endregion //BaseCSVImporter Implementation

    }

}