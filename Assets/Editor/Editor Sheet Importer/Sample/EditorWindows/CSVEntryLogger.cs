using UnityEditor;
using UnityEngine;

namespace UnitySheetImporter.Sample
{

	public class CSVEntryLogger : BaseCSVReader
	{

		#region Static fields

		private const string TITLE = "CSV Entry [Logger]";

		#endregion

		#region EditorWindow Implementation

		[MenuItem(Constants.EDITOR_WINDOW_SAMPLE + TITLE)]
		private static void ShowWindow()
		{
			GetWindow<CSVEntryLogger>(false, TITLE, true);
		}

		#endregion

		#region BaseCSVReader Implementation

		protected override bool IsRuntimeOnly() => false;

		protected override string GetButtonText() => "LOG";

		protected override void OnReadCSVEntry(string[] entry)
		{
			Debug.Log($"{GetType().Name}: Entry in CSV: | {string.Join(" | ", entry)}");
			AddProcessedLineCount();
			FinishCurrentLine();
		}

		protected override void OnReadCSVFirstRow(string[] firstRow)
		{
			Debug.Log($"{GetType().Name}: <color=lime>" +
				$"Column Names in CSV:</color> {string.Join(" | ", firstRow)}");
			FinishCurrentLine();
		}

		#endregion
	}

}