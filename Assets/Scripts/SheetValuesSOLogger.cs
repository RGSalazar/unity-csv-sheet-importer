using UnityEngine;

namespace UnitySheetImporter.Sample
{

    public class SheetValuesSOLogger : MonoBehaviour
    {

        [SerializeField]
        private PeopleListConfig config;

        private void Start()
        {
            foreach (var person in config.listPeople)
            {
                Debug.Log(person.name);
            }
        }

    }

}