# Unity CSV Sheet Importer

### This is a project that contains a tool that reads and parses values from a CSV sheet into data for Unity codes consumption. It's ideal for level design and calibrations.

To use, use namespace `UnitySheetImporter` and make `BaseCSVImporter` the parent of your own class. Check out the said parent class's code for updated, commented documentations.

You can also check out on the Project files:
`Assets > Editor > Editor Sheet Importer > Sample > EditorWindows > PeopleCSVImporter`
for an actual usage of the parent class and a clear guide on the application of its abstract methods.

To test the sample EditorWindow available in the project,

[1] On Unity Editor, go to the topbar:
`Unity Sheet Importer > Sample > People CSV Importer`

[2] An EditorWindow will pop up and it will ask for two things:
- [A] **Scriptable Object to Update** (you can use the readily-available SOs in the project for this; notice that the list it contains is empty)
- [B] **CSV File Path** (you can click the `"..."` button on the right side of the popup window to look for any CSV to use. Ideally, for the sample, use the readily-available CSV in the project files:
`Assets > Editor > Editor Sheet Importer > Sample`

NOTE: You can also check the **NOTE.txt** for extra instructions.

[3] Click the **"Import"** button on the popup window.

_Notice that the Scriptable Object you indicated in the form will be filled with values that were read by the popup window and parsed into data that Unity can now display. You can then use these data in your project for anything._